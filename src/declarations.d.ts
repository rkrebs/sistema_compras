declare namespace JSX {
  interface IntrinsicElements {
    "br-button": any;
    "br-checkbox": any;
    "br-footer": any;
    "br-header": any;
    "br-input": any;
    "br-menu": any;
    "br-message": any;
  }
}
