const Footer = () => {
  const text =
    "Todo o conteúdo deste site está publicado sob a licença CC0 1.0 Universal";

  const social = `{
      'label': 'Redes sociais',
      'networks': [
        {
          'href': 'https://discord.gg/U5GwPfqhUP',
          'image': 'discord.svg',
          'description': 'Discord',
          'target':'_blank'
        }
      ]
    }`;

  return <br-footer text={text} social={social}></br-footer>;
};

export default Footer;
